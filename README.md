pada terminal :

$use npm install --global lerna
$lerna init --independent

pada tutorial kali ini kita akan membuat microservices menggunakan

nodeJS sebagai server
graphql sebagai API Gateway
mongoDB sebagai database service dimana kita akan menggunakan mongoose untuk mengintegrasikan DB dan aplikasi
PM2 sebagai service wathcer , clustering , controlling dan monitoring
RabbitMQ dari https://www.cloudamqp.com/ sebagai QUEUE message dan load balancer yang terhubung graphQL dan mailjet services
dan mailJET sebagai Mailer services yang akan mengambil data pada RabbitMQ


-untuk memulai:
pada terminal jalankan pm2 start ecosystem.config.js

-untuk menghentikan : 
pm2 stop --all 
pm2 delete all

-untuk monitoring sistem
pm2 monit

RabbitMQ
node packages/gateway/Q/connect.js untuk menjalankan rabbitMQ

node packages/gateway/consumer.js untuk melihat data queue pada terminal


GraphQL

GraphQL has a clear separation of structure and behaviour. The structure of a GraphQL server is — as we just discussed — its schema, an abstract description of the server’s capabilities. This structure comes to life with a concrete implementation that determines the server’s behaviour. Key components for the implementation are so-called resolver functions.

Each field in a GraphQL schema is backed by a resolver.

In its most basic form, a GraphQL server will have one resolver function per field in its schema. Each resolver knows how to fetch the data for its field. Since a GraphQL query at its essence is just a collection of fields, all a GraphQL server actually needs to do in order to gather the requested data is invoke all the resolver functions for the fields specified in the query. (This is also why GraphQL often is compared to RPC-style systems, as it essentially is a language for invoking remote functions.)

untuk melihat graph ql : http://localhost:<PORT>/gq

PM2
pm2 : process manager digunakan untuk kontrol ,monitoring , watch service ,menjalankan aplikasi menggunaan cluster mode