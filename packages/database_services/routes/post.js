const mongoose = require('mongoose');
const Mail = mongoose.model('Mail');

const mailhandler = async ({body: {subject, receiver,content}},res) =>{
    let mail;
    let error;

    if(!subject || !receiver || !content ){
        res.sendStatus(400).send({
            message: 'you forgot some import key',
            service: 'database service',
            status: 400,
            payload: null
        })
    }
    const newMail = new Mail({
        subject,
        receiver,
        content
    })

    try{
        mail = await newMail.save();
    } catch (err){
        error = err
    }

    res.send({
        message: 'Got Response from DB',
        service: 'database service',
        status: 200,
        payload : mail || err
    });
};



module.exports = server => {
    server
    .post('/mails',mailhandler);
}