const { PORT } = process.env

module.exports = {
    port: PORT || 4000,
    mongoURI: 'mongodb+srv://topgun:tr1tr0n1kdb@microservicesdb-wbrzc.mongodb.net/test?retryWrites=true&w=majority'
}