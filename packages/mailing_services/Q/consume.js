const amqp = require('amqplib/callback_api');
const { q: { uri } } = require('../config')

const sendMail = require('../handler/sendMail')

module.exports = () => {
    const q = 'test_q';

amqp.connect(uri, (err, conn) => {
    if (err) throw new Error(err);

    conn.createChannel((err, ch) => {
        if (err) throw new Error(err);
        ch.assertQueue(q, { durable: true }) // durable digunakan untuk menyimpan message dan akan dihapus ketika service meminta dihapus , fungsinya agar data tidak hilang ketika queue mengrim data namun service sedang error

        // ch.sendToQueue(q, Buffer.from('Hello TEST CONSUMER')); //perintah untuk menyimpan data ke queue

        ch.consume(q, msg => {
            let mail;

            try {
                mail = JSON.parse(msg.content.toString())
            } catch (e) {
                console.log(e);

                mail = content.toString();
            }
            
            console.log('I RECEIVED A MAIL!!',mail)
            sendMail(mail)
            ch.ack(msg);

        },
        {noAck: false})
    });
});

}

