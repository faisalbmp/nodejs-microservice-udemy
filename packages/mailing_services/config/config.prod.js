const { PORT, Q_URI } = process.env

module.exports = {
    port: PORT || 3000,
    q: {
        uri: Q_URI || 'bbalbal'
    }
}