const amqp = require('amqplib/callback_api');

const q = 'test_q';

amqp.connect('amqp://vlfezanb:kCFPPdeDLBmH9IJjRmIuP48Le5A4Csd-@skunk.rmq.cloudamqp.com/vlfezanb',(err,conn)=>{
    if (err) throw new Error(err);

    conn.createChannel((err,ch)=>{
        if(err) throw new Error(err);

        ch.assertQueue(q,{durable:true})
        ch.consume(q,msg=>{  // ch.consume digunakan untuk menampilkan isi data pada queue
            console.log('I GOT MESSAGE',msg.content.toString())
        },
        {noAck:true})//noAck untuk memerintah rabbitMQ untuk menghapus isi data queue yang telah diterima        
    })
});

/* -------------------------------------------------------

    consumer.js merupakan data script untuk menampilkan queue pada terminal

    jalankan terlebih dahulu pada terminal : node Q/connect.js
    kemudian jalankan pada terminal : node consumer.js 

-------------------------------------------------------*/