const amqp = require('amqplib/callback_api');
const { q: { uri } } = require('../config')

const q = 'test_q';
/* q adalah queue name dan string adalah identifier */
let channel;

/* -------------------------------------------------------------------------------------------------------------

    pertama establish koneksi dengan rabbitMQ kemudian rabbitMQ  akan mencari q yang dinamakan test_q ,
    jika ada queue yang dinamakan test_q maka aplikasi berjalan jika tidak ada maka aplikasi akan memerintah rabbit mq untuk membuat queue yang dinamakan test_q
 
 -------------------------------------------------------------------------------------------------------------------------*/

amqp.connect(uri, (err, conn) => {
    if (err) throw new Error(err);

    conn.createChannel((err, ch) => {
        if (err) throw new Error(err);
        ch.assertQueue(q, { durable: true }) // durable digunakan untuk menyimpan message dan akan dihapus ketika service meminta dihapus , fungsinya agar data tidak hilang ketika queue mengrim data namun service sedang error

        // ch.sendToQueue(q, Buffer.from('Hello TEST CONSUMER')); //perintah untuk menyimpan data ke queue
        channel = ch;
    });
});

const pushToMessageQ = msg => { //pada parameter bisa ditambakan (msg,channel,q) jika terdapat banyak queue / q
    if (!channel) setTimeout(pushToMessageQ(msg), 1000);

    channel.sendToQueue(q, Buffer.from(msg))

    return { m: 'done' };
}

module.exports = {
    pushToMessageQ
}

/* ---------------------------------------------------
    untuk menjalankan :
    node Q/connect.js

    untuk melihat buka rabbitMQ manager pilih tab queue
    dan untuk melihat lebih detail klik nama queue (test_q)
---------------------------------------------------*/