const server = require('express')();
const {graphqlExpress, graphiqlExpress} = require('apollo-server-express');
const bodyParser = require('body-parser'); // body-parser middleware pada express
const schema = require('./data/schema')
/* ---------------------------------
    body-parser = merupakan modul sebagai middleware pada express
    graphqlexpress = graphql module for express
    graphiqlexpress = interface that graphql or apollo provide ,
                    actually it like some kind of id that giving
                    description tha we define in schema
 -----------------------------------*/

const {port} = require('./config');

/* === untuk mencoba config dengan mengubah port pada terminal ketik : $PORT=3001 node server.js == */


server
.use(bodyParser.json())
.use('/graphql',graphqlExpress({schema}))
.use('/gq',graphiqlExpress({endpointURL:'/graphql'}))
.listen(port,()=> console.log(`listening on port Hello ${port}`));

/* ----------------------------------------------------------
    pada terminal $PORT=3000 nodemon server.js
    buka browser dan masukkan url: localhost://localhost:3000/gq
    kemudian ketik pada kolom yang disediakan sebelah kiri
    query{
       hey
    }
-------------------------------------------------------------*/