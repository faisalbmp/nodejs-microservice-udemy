const path = require('path');

const basePath = path.join(__dirname, '/packages');

module.exports = {
  apps: [
    {
      name: 'Gateway',
      script: basePath + '/gateway/server.js',

      // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
      watch: true,
      env: {
        PORT: 3001,
        SERVICE_DB_PORT: 4001,
        Q_URI: 'amqp://vlfezanb:kCFPPdeDLBmH9IJjRmIuP48Le5A4Csd-@skunk.rmq.cloudamqp.com/vlfezanb'
      }
    },
    {
      name: 'DB Service',
      script: basePath + '/database_services/server.js',
      watch: true,
      env: {
        PORT: 4001
      }
    },
    {
      name: 'Mailing Service',
      script: basePath + '/mailing_services/index.js',
      watch: true,
      env: {
        MJ_API_PUBLIC:'7f814bfbfe6f305b174d003bf530574e',
        MJ_API_SECRET:'398e03f0be4c947906b8709384969c79',
        Q_URI: 'amqp://vlfezanb:kCFPPdeDLBmH9IJjRmIuP48Le5A4Csd-@skunk.rmq.cloudamqp.com/vlfezanb',
        PORT: 4001
      }
    }
  ]
};

/* -------------------------

pm2 : process manager digunakan untuk kontrol ,monitoring , watch service ,menjalankan aplikasi menggunaan cluster mode

-untuk memulai:
pada terminal jalankan pm2 start ecosystem.config.js

-untuk menghentikan :
pm2 stop --all
pm2 delete all

-untuk monitoring sistem
pm2 monit
-------------------------------*/